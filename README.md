# Comandos que usaremos: 

Los comandos que usaremos serán los siguientes:

* git init **para inicializar un repositorio.**
* git add <file> **para agregar un repositorio del working area al staging area.**
* git status **para ver el estado de nuestro proyecto.**
* git commit **para agregar un repositorio del staging area al repositorio.**
* git push **para subir los archivos a un repositorio remoto.**
* git clone **para clonar un repositorio.**

* Y alguno mas que veremos conforme avance la clase ;)

*No os preocupeis, una vez le pillas el truquillo es bastante fácil.*

**Hecho por Josito**